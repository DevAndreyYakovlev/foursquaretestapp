package com.ex.foursquareapp.di.venues_list;

import com.ex.foursquareapp.ui.venues.venues_list.view.VenuesListFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Subcomponent(modules = {VenuesListModule.class})
@VenuesListScope
public interface VenuesListComponent {
    void inject(VenuesListFragment venuesListFragment);
}
