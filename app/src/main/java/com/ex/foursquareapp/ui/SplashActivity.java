package com.ex.foursquareapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ex.foursquareapp.R;
import com.ex.foursquareapp.ui.main_screen.view.MainScreenActivity;
import com.ex.foursquareapp.utils.BaseActivity;

public class SplashActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_main);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, MainScreenActivity.class));
            finish();
        }, 1500);

    }


}
