package com.ex.foursquareapp.ui.venues.venues_map.presenter;

import com.ex.foursquareapp.business.venues_map.IVenuesMapInteractor;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.ui.venues.venues_map.view.IVenuesMapView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenuesMapPresenter implements IVenuesMapPresenter {


    private final IVenuesMapInteractor mVenuesMapInteractor;
    private IVenuesMapView mMapView;

    public VenuesMapPresenter(IVenuesMapInteractor venuesMapInteractor) {

        mVenuesMapInteractor = venuesMapInteractor;
    }

    @Override
    public void bindView(IVenuesMapView venuesMapFragment) {
        mMapView = venuesMapFragment;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void getVenuesData() {
        mVenuesMapInteractor.loadData()
            .subscribe(venuesEntities -> {
                mMapView.showPlaceMarks(venuesEntities);
            });
    }
}
