package com.ex.foursquareapp.data.entity.venues;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public  class Location extends RealmObject {

    @SerializedName("address")
    private String address;

    @SerializedName("lat")
    private double lat;

    @SerializedName("distance")
    private double distance;

    @SerializedName("lng")
    private double lng;

    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
