package com.ex.foursquareapp.di.main_screen;

import com.ex.foursquareapp.ui.main_screen.view.MainScreenActivity;

import dagger.Subcomponent;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Subcomponent(modules = {MainScreenModule.class})
public interface MainScreenComponent {
    void inject(MainScreenActivity mainScreenActivity);
}
