package com.ex.foursquareapp.di.application;

import com.ex.foursquareapp.di.main_screen.MainScreenComponent;
import com.ex.foursquareapp.di.main_screen.MainScreenModule;
import com.ex.foursquareapp.di.venue_window.VenueWindowComponent;
import com.ex.foursquareapp.di.venue_window.VenueWindowModule;
import com.ex.foursquareapp.di.venues_list.VenuesListComponent;
import com.ex.foursquareapp.di.venues_list.VenuesListModule;
import com.ex.foursquareapp.di.venues_map.VenuesMapComponent;
import com.ex.foursquareapp.di.venues_map.VenuesMapModule;
import com.ex.foursquareapp.di.venues_module.VenuesModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Component(modules = {AppModule.class, FoursquareApiModule.class, VenuesModule.class})
@Singleton
public interface AppComponent {
    VenuesListComponent plus(VenuesListModule venuesListModule);
    MainScreenComponent plus(MainScreenModule mainScreenModule);
    VenuesMapComponent plus(VenuesMapModule venuesMapModule);
    VenueWindowComponent plus(VenueWindowModule venueWindowModule);
}
