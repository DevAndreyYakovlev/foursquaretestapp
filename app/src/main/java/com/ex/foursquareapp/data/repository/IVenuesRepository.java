package com.ex.foursquareapp.data.repository;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface IVenuesRepository {
    Observable<List<VenuesEntity>> loadDataByLocation(LatLng latLng);

    boolean venuesDataIsEmpty();

    Observable<List<VenuesEntity>> loadData();

    Observable<VenuesEntity> loadVenueById(String id);

    void removeVenue(VenuesEntity venuesEntity);
}
