package com.ex.foursquareapp.business.venues;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.data.repository.IVenuesRepository;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesInteractor implements IVenuesInteractor {
    private final IVenuesRepository mVenuesRepository;

    public VenuesInteractor(IVenuesRepository venuesRepository) {
        this.mVenuesRepository = venuesRepository;
    }

    @Override
    public Observable<List<VenuesEntity>> loadDataByLocation(LatLng latLng) {
        return mVenuesRepository.loadDataByLocation(latLng);
    }

    @Override
    public boolean venuesDataIsEmpty() {
        return mVenuesRepository.venuesDataIsEmpty();
    }

    @Override
    public Observable<List<VenuesEntity>> loadData() {
        return mVenuesRepository.loadData();
    }

    @Override
    public Observable<VenuesEntity> loadVenueById(String id) {
        return mVenuesRepository.loadVenueById(id);
    }

    @Override
    public void removeVenue(VenuesEntity venuesEntity) {
        mVenuesRepository.removeVenue(venuesEntity);
    }
}
