package com.ex.foursquareapp.di.venue_window;

import com.ex.foursquareapp.ui.venues.venue_window.view.VenueActivity;

import dagger.Subcomponent;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

@Subcomponent(modules = {VenueWindowModule.class})
@VenueWindowScope
public interface VenueWindowComponent {
    void inject(VenueActivity venueActivity);
}
