package com.ex.foursquareapp.data.entity.venues;

import com.ex.foursquareapp.data.entity.venues.categories.Categories;
import com.ex.foursquareapp.data.entity.venues_image.VenuesImage;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesEntity extends RealmObject {

    @SerializedName("id")
    @PrimaryKey
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private Location location;

    @SerializedName("categories")
    private RealmList<Categories> categories;

    @SerializedName("contact")
    private Contact contact;

    @SerializedName("url")
    private String url;

    private RealmList<VenuesImage> venuesImage;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public RealmList<Categories> getCategories() {
        return categories;
    }

    public void setCategories(RealmList<Categories> categories) {
        this.categories = categories;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RealmList<VenuesImage> getVenuesImage() {
        return venuesImage;
    }

    public void setVenuesImage(RealmList<VenuesImage> venuesImage) {
        this.venuesImage = venuesImage;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VenuesEntity)) return false;

        VenuesEntity that = (VenuesEntity) o;

        if (!id.equals(that.id)) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "VenuesEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
