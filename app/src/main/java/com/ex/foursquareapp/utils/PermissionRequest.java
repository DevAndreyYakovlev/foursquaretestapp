package com.ex.foursquareapp.utils;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class PermissionRequest {

    boolean mGranted;

    public PermissionRequest(Boolean granted) {
        mGranted = granted;
    }
}
