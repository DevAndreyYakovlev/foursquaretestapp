package com.ex.foursquareapp.di.venue_window;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface VenueWindowScope {
}
