package com.ex.foursquareapp.ui.venues.venues_list.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.ex.foursquareapp.R;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.data.entity.venues_image.VenuesImage;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesRecyclerViewAdapter extends RecyclerView.Adapter<VenuesRecyclerViewAdapter.MyViewHolder> {

    private List<VenuesEntity> mVenuesEntities;
    private final Context mContext;
    private BehaviorSubject<VenuesEntity> onClickVenuesSubject = BehaviorSubject.create();
    private BehaviorSubject<VenuesEntity> removeItemSubject = BehaviorSubject.create();

    public VenuesRecyclerViewAdapter(Context context){
        mVenuesEntities = new ArrayList<>();
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.venues_recycler_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        VenuesEntity venuesEntity = mVenuesEntities.get(position);
        String title = "";
        String subTitle = "";
        String url = "";
        if (venuesEntity.getCategories().size() > 0){
            title = venuesEntity.getName() + " \u2022 " + venuesEntity.getCategories().get(0).getName();
        }else{
            title = venuesEntity.getName();
        }
        if (venuesEntity.getVenuesImage().size() > 0){
            url = venuesEntity.getVenuesImage().get(0).getPrefix().concat("400x400").concat(venuesEntity.getVenuesImage().get(0).getSuffix());
        }else if (venuesEntity.getCategories().size() > 0){
            url = venuesEntity.getCategories().get(0).getIcon().getPrefix().concat("88").concat(venuesEntity.getCategories().get(0).getIcon().getSuffix());
            if  (!url.isEmpty()){
                Picasso.with(mContext)
                        .load(url)
                        .into(holder.categoryIcon);
            }else{

            }
        }

        if (!url.isEmpty()){
            Picasso.with(mContext)
                .load(url)
                .into(holder.venuesPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.photoProgressBarContainer.animate().alpha(0).setDuration(300);
                    }

                    @Override
                    public void onError() {

                    }
                });
        }else{
            holder.venuesPhotoProgressBar.setVisibility(View.GONE);
            holder.venuesLogo.setVisibility(View.VISIBLE);
        }

        if (venuesEntity.getLocation() != null){
            subTitle = venuesEntity.getLocation().getAddress();
        }
        holder.venuesTitle.setText(title);
        holder.venuesSubTitle.setText(subTitle);

        holder.rootElement.setOnClickListener(v -> {
            onClickVenuesSubject.onNext(venuesEntity);
        });
        holder.mMenuImage.setOnClickListener(v -> {
            PopupMenu popup = new PopupMenu(mContext, holder.mMenuImage);
            //inflating menu from xml resource
            popup.inflate(R.menu.options_menu);
            //adding click listener
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        //handle menu1 click
                        removeItemSubject.onNext(venuesEntity);
                        removeAt(position);
                        break;

                }
                return false;
            });
            //displaying the popup
            popup.show();
        });

    }
    public void removeAt(int position) {
        mVenuesEntities.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mVenuesEntities.size());
    }

    public BehaviorSubject<VenuesEntity> getOnClickVenuesSubject(){
        return onClickVenuesSubject;
    }

    public BehaviorSubject<VenuesEntity> getRemoveItemSubject(){
        return removeItemSubject;
    }

    @Override
    public int getItemCount() {
        return mVenuesEntities.size();
    }

    public void setVenuesData(List<VenuesEntity> venuesEntities) {
        mVenuesEntities.clear();
        mVenuesEntities.addAll(venuesEntities);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        @BindView(R.id.venuesTitle) TextView venuesTitle;
        @BindView(R.id.venuesSubTitle) TextView venuesSubTitle;
        @BindView(R.id.venuesPhoto) ImageView venuesPhoto;
        @BindView(R.id.venuesPhotoProgressBarContainer) FrameLayout photoProgressBarContainer;
        @BindView(R.id.venuesProgressBarView) ProgressBar venuesPhotoProgressBar;
        @BindView(R.id.venuesLogo) ImageView venuesLogo;
        @BindView(R.id.venuesItemRootElement) LinearLayout rootElement;
        @BindView(R.id.itemMenu) ImageView mMenuImage;
        @BindView(R.id.venuesCategoryIcon) ImageView categoryIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }
}
