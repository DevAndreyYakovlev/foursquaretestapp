package com.ex.foursquareapp.ui.main_screen.view;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface IMainScreenView {

    void showSnackBar(String s);
    void showListScreen();
    void showMapScreen();

}
