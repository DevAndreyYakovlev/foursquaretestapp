package com.ex.foursquareapp.di.venues_map;

import com.ex.foursquareapp.ui.venues.venues_map.view.VenuesMapFragment;

import dagger.Subcomponent;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */
@Subcomponent(modules = {VenuesMapModule.class})
@VenuesMapScope
public interface VenuesMapComponent {
    void inject(VenuesMapFragment venuesMapFragment);
}
