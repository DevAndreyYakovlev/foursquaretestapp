package com.ex.foursquareapp.ui.main_screen.presenter;

import android.support.design.widget.TabLayout;

import com.ex.foursquareapp.ui.main_screen.view.IMainScreenView;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class MainScreenPresenter implements IMainScreenPresenter {

    private IMainScreenView mMainScreenView;

    @Override
    public void bindView(IMainScreenView mainScreenView) {
        mMainScreenView = mainScreenView;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0){
            mMainScreenView.showListScreen();
        }else{
            mMainScreenView.showMapScreen();
        }
    }


}
