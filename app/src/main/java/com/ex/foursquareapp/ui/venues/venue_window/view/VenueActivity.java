package com.ex.foursquareapp.ui.venues.venue_window.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ex.foursquareapp.FoursquareApp;
import com.ex.foursquareapp.R;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.data.entity.venues_image.VenuesImage;
import com.ex.foursquareapp.di.venue_window.VenueWindowModule;
import com.ex.foursquareapp.ui.venues.venue_window.presenter.IVenueWindowPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenueActivity extends AppCompatActivity implements IVenueWindowView {

    @Inject IVenueWindowPresenter mVenueWindowPresenter;
    @BindView(R.id.venueStreetName) TextView mVenueStreetName;
    @BindView(R.id.venuePhoneNumber) TextView mVenuePhoneNumber;
    @BindView(R.id.venueCategory) TextView mVenueCategory;
    @BindView(R.id.venueDistance) TextView mVenueDistance;
    @BindView(R.id.venueViewPager) ViewPager mVenueViewPager;
    @BindView(R.id.venueWebSite) TextView mVenueWebSite;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private VenueViewPagerAdapter mViewPagerAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.venue_activity);
        FoursquareApp.get(getApplicationContext()).applicationComponent().plus(new VenueWindowModule()).inject(this);
        ButterKnife.bind(this);
        mVenueWindowPresenter.bindView(this);
        mVenueWindowPresenter.getVenueDataById((String) getIntent().getExtras().get("venueId"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        }
    }

    private void initToolBar(String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {finish();});
    }

    @Override
    public void showVenueData(VenuesEntity venuesEntity) {
        mVenueStreetName.setText(venuesEntity.getName());

        initToolBar(venuesEntity.getName());
        if (venuesEntity.getContact().getPhone() != null){
            mVenuePhoneNumber.setText(venuesEntity.getContact().getFormattedPhone());
        }else{
            findViewById(R.id.venuePhoneContainer).setVisibility(View.GONE);
        }
        if (venuesEntity.getCategories().size() > 0){
            mVenueCategory.setText(venuesEntity.getCategories().get(0).getName());
        }else{
            findViewById(R.id.venueCategoryContainer).setVisibility(View.GONE);
        }

        mVenueDistance.setText(String.valueOf((int)venuesEntity.getLocation().getDistance()).concat(" м."));

        if (venuesEntity.getVenuesImage().size() > 0){
            mViewPagerAdapter = new VenueViewPagerAdapter(getSupportFragmentManager());
            mVenueViewPager.setAdapter(mViewPagerAdapter);
            if (venuesEntity.getVenuesImage().size() > 2){
                int sz = venuesEntity.getVenuesImage().size() / 2;
                List<VenuesImage> venuesImages = new ArrayList<>();
                for (int i = 0; i < venuesEntity.getVenuesImage().size() - sz; i++){
                    venuesImages.add(venuesEntity.getVenuesImage().get(i));
                }
                mViewPagerAdapter.addPhotos(venuesImages);
            }else{
                mViewPagerAdapter.addPhotos(venuesEntity.getVenuesImage());
            }
        }
        if (venuesEntity.getUrl() != null){
            mVenueWebSite.setText(venuesEntity.getUrl());
            mVenueWebSite.setOnClickListener(v -> {
                String url = venuesEntity.getUrl();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            });
        }else{
            findViewById(R.id.venueWebSiteContainer).setVisibility(View.GONE);
        }
    }
}
