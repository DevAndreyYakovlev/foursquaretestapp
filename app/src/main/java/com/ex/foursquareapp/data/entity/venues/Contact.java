package com.ex.foursquareapp.data.entity.venues;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class Contact extends RealmObject {

    @SerializedName("phone")
    private String phone;
    @SerializedName("formattedPhone")
    private String formattedPhone;
    @SerializedName("twitter")
    private String twitter;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }
}
