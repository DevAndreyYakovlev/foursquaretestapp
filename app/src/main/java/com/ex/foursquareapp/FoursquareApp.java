package com.ex.foursquareapp;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.ex.foursquareapp.di.application.AppComponent;
import com.ex.foursquareapp.di.application.AppModule;
import com.ex.foursquareapp.di.application.DaggerAppComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class FoursquareApp extends Application {

    @SuppressWarnings("NullableProblems")
    @NonNull
    private AppComponent appComponent;

    public static FoursquareApp get(@NonNull Context context){
        return (FoursquareApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = prepareAppComponent().build();
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfig);
    }


    @NonNull
    private DaggerAppComponent.Builder prepareAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this));
    }

    @NonNull
    public AppComponent applicationComponent() {
        return appComponent;
    }

}
