package com.ex.foursquareapp.utils.zoomer;

public interface IDisposable {

    void dispose();
}
