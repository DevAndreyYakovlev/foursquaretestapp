package com.ex.foursquareapp.ui.venues.venue_window.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ex.foursquareapp.data.entity.venues_image.VenuesImage;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by Yakovlev Andrey.
 */

public class VenueViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<VenuesImage> mPhotos;

    public VenueViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mPhotos = new ArrayList<>();
    }

    public void addPhotos(List<VenuesImage> photos){
        mPhotos.addAll(photos);
        this.notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return new VenueViewPagerItem(mPhotos.get(position));
    }

    @Override
    public int getCount() {
        return mPhotos.size();
    }
}
