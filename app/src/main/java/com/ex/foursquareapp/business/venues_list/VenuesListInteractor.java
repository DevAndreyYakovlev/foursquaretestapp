package com.ex.foursquareapp.business.venues_list;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesListInteractor implements IVenuesListInteractor {
    private final IVenuesInteractor mVenuesInteractor;

    public VenuesListInteractor(IVenuesInteractor venuesInteractor) {
        mVenuesInteractor = venuesInteractor;
    }

    @Override
    public Observable<List<VenuesEntity>> loadDataByLocation(LatLng latLng) {
        return mVenuesInteractor.loadDataByLocation(latLng);
    }

    @Override
    public boolean venuesDataIsEmpty() {
        return mVenuesInteractor.venuesDataIsEmpty();
    }

    @Override
    public Observable<List<VenuesEntity>> loadData() {
        return mVenuesInteractor.loadData();
    }

    @Override
    public void removeVenue(VenuesEntity venuesEntity) {
        mVenuesInteractor.removeVenue(venuesEntity);
    }
}
