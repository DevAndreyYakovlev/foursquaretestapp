package com.ex.foursquareapp.di.venues_map;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.business.venues_list.IVenuesListInteractor;
import com.ex.foursquareapp.business.venues_list.VenuesListInteractor;
import com.ex.foursquareapp.business.venues_map.IVenuesMapInteractor;
import com.ex.foursquareapp.business.venues_map.VenuesMapInteractor;
import com.ex.foursquareapp.di.venues_list.VenuesListScope;
import com.ex.foursquareapp.ui.venues.venues_list.persenter.IVenuesListPresenter;
import com.ex.foursquareapp.ui.venues.venues_list.persenter.VenuesListPresenter;
import com.ex.foursquareapp.ui.venues.venues_map.presenter.IVenuesMapPresenter;
import com.ex.foursquareapp.ui.venues.venues_map.presenter.VenuesMapPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */
@Module
public class VenuesMapModule {

    @Provides
    @VenuesMapScope
    IVenuesMapPresenter provideVenuesListPresenter(IVenuesMapInteractor venuesMapInteractor){
        return new VenuesMapPresenter(venuesMapInteractor);
    }

    @Provides
    @VenuesMapScope
    IVenuesMapInteractor provideVenuesMapInteractor(IVenuesInteractor venuesInteractor){
        return new VenuesMapInteractor(venuesInteractor);
    }

}
