package com.ex.foursquareapp.data.entity.venues_image;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenuesImage extends RealmObject {

    @SerializedName("id")
    private String id;

    @SerializedName("prefix")
    private String prefix;

    @SerializedName("suffix")
    private String suffix;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
