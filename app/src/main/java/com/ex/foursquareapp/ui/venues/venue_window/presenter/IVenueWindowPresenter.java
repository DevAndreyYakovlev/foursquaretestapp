package com.ex.foursquareapp.ui.venues.venue_window.presenter;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.ui.venues.venue_window.view.IVenueWindowView;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public interface IVenueWindowPresenter {

    void bindView(IVenueWindowView venueWindowView);
    void unbindView();
    void getVenueDataById(String id);
}
