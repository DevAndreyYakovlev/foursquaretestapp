package com.ex.foursquareapp.di.application;

import android.content.Context;

import com.ex.foursquareapp.utils.retrofit.FoursquareApiService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Module
public class FoursquareApiModule {

    String mBaseUrl = "https://api.foursquare.com/v2/";
    private Context application;
        private static final int TIMEOUT = 15;


    @Provides
    @Singleton
    Cache provideHttpCache(Context appContext) {
        int cacheSize = 10 * 1024 * 1024;
        this.application = appContext;

        return new Cache(appContext.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }
    @Provides
    OkHttpClient provideOkhttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS);
        return client.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    FoursquareApiService provideKvAppApi(Retrofit retrofit){
        return retrofit.create(FoursquareApiService.class);
    }

}
