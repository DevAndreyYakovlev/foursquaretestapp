package com.ex.foursquareapp.ui.venues.venue_window.view;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public interface IVenueWindowView {

    void showVenueData(VenuesEntity venuesEntity);

}
