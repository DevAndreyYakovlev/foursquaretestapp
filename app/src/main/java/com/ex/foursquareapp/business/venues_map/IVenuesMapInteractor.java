package com.ex.foursquareapp.business.venues_map;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public interface IVenuesMapInteractor {


    Observable<List<VenuesEntity>> loadData();

}
