package com.ex.foursquareapp.business.venues_list;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface IVenuesListInteractor {
    Observable<List<VenuesEntity>> loadDataByLocation(LatLng latLng);

    boolean venuesDataIsEmpty();

    Observable<List<VenuesEntity>> loadData();

    void removeVenue(VenuesEntity venuesEntity);
}
