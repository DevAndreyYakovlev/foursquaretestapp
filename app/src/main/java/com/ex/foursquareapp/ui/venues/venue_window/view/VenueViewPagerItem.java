package com.ex.foursquareapp.ui.venues.venue_window.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.ex.foursquareapp.R;
import com.ex.foursquareapp.data.entity.venues_image.VenuesImage;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yakovlev Andrey.
 */

public class VenueViewPagerItem extends Fragment {

    private View mRootView;
    private VenuesImage mPhoto;
    @BindView(R.id.flatSlideImg) ImageView mVenueSlideImage;

    @SuppressLint("ValidFragment")
    public VenueViewPagerItem(VenuesImage photo) {
        mPhoto = photo;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.apartment_view_pager_item, container, false);
        ButterKnife.bind(this, mRootView);
        String url = mPhoto.getPrefix().concat("400x400").concat(mPhoto.getSuffix());
        Glide.with(this).load(url).into(mVenueSlideImage);

        mVenueSlideImage.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), VenuePhotoZoomActivity.class);
            intent.putExtra("url", url);
            startActivity(intent);
        });

        return mRootView;
    }
}
