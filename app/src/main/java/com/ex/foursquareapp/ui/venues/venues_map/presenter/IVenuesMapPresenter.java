package com.ex.foursquareapp.ui.venues.venues_map.presenter;

import com.ex.foursquareapp.ui.venues.venues_map.view.IVenuesMapView;
import com.ex.foursquareapp.ui.venues.venues_map.view.VenuesMapFragment;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public interface IVenuesMapPresenter {
    void bindView(IVenuesMapView venuesMapFragment);
    void unbindView();

    void getVenuesData();
}
