package com.ex.foursquareapp.utils.retrofit;

import com.ex.foursquareapp.data.entity.venues_image.VenuesImage;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenuesImageResponse {

    @SerializedName("items")
    private ArrayList<VenuesImage> venuesImages;

    public ArrayList<VenuesImage> getVenuesImages() {
        return venuesImages;
    }

    public void setVenuesImages(ArrayList<VenuesImage> venuesImages) {
        this.venuesImages = venuesImages;
    }
}
