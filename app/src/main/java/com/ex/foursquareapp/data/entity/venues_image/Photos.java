package com.ex.foursquareapp.data.entity.venues_image;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class Photos {

    @SerializedName("items")
    private RealmList<VenuesImage> venuesImages;

    public RealmList<VenuesImage> getVenuesImages() {
        return venuesImages;
    }

    public void setVenuesImages(RealmList<VenuesImage> venuesImages) {
        this.venuesImages = venuesImages;
    }
}
