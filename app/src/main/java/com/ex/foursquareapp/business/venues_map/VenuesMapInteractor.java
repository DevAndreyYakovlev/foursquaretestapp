package com.ex.foursquareapp.business.venues_map;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenuesMapInteractor implements IVenuesMapInteractor {


    private final IVenuesInteractor mVenuesInteractor;

    public VenuesMapInteractor(IVenuesInteractor venuesInteractor) {
        mVenuesInteractor = venuesInteractor;
    }

    @Override
    public Observable<List<VenuesEntity>> loadData() {
        return mVenuesInteractor.loadData();
    }
}
