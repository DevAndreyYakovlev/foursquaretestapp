package com.ex.foursquareapp.ui.venues.venues_list.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ex.foursquareapp.FoursquareApp;
import com.ex.foursquareapp.R;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.di.venues_list.VenuesListModule;
import com.ex.foursquareapp.ui.venues.venue_window.view.VenueActivity;
import com.ex.foursquareapp.ui.venues.venues_list.persenter.IVenuesListPresenter;
import com.ex.foursquareapp.utils.RxPermissionListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;


import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesListFragment extends Fragment implements IVenuesListView {

    private View mRootView;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    @Inject IVenuesListPresenter mVenuesListPresenter;
    private VenuesRecyclerViewAdapter mVenuesRecyclerViewAdapter;
    @BindView(R.id.venuesRecyclerView) RecyclerView mVenuesRecyclerView;
    @BindView(R.id.progressBarContainer) FrameLayout mProgressBarContainer;
    @BindView(R.id.progressBarDescription) TextView mProgressBarDescription;
    @BindView(R.id.permissionBanner) FrameLayout mPermissionBanner;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean dataLoadedFlag;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FoursquareApp.get(getContext()).applicationComponent().plus(new VenuesListModule()).inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.venues_list_fmt, container, false);
        ButterKnife.bind(this, mRootView);

        mVenuesRecyclerViewAdapter = new VenuesRecyclerViewAdapter(getContext());
        mVenuesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mVenuesRecyclerView.setAdapter(mVenuesRecyclerViewAdapter);


        mVenuesListPresenter.bindView(this);
        if (mVenuesListPresenter.venuesDataIsEmpty()){
            RxPermissionListener.replaySubject
                    .subscribe(aBoolean -> {
                        if (!aBoolean){
                            mPermissionBanner.setVisibility(View.VISIBLE);
                        }else{
                            mPermissionBanner.setVisibility(View.GONE);
                            myLocationListener();
                        }
                    });
        }else{
            mPermissionBanner.setVisibility(View.GONE);
            mProgressBarContainer.setVisibility(View.GONE);
            mVenuesListPresenter.loadData();
        }

        mVenuesRecyclerViewAdapter.getOnClickVenuesSubject()
                .subscribe(venuesEntity -> {
                    Intent intent = new Intent(getContext(), VenueActivity.class);
                    intent.putExtra("venueId", venuesEntity.getId());
                    getActivity().startActivity(intent);
                });

        mVenuesRecyclerViewAdapter.getRemoveItemSubject()
                .subscribe(venuesEntity -> {
                    mVenuesListPresenter.removeVenue(venuesEntity);
                });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            myLocationListener();
        });

        return mRootView;
    }

    @Override
    public void showVenues(List<VenuesEntity> venuesEntities) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (venuesEntities.size() > 0){
            mVenuesRecyclerViewAdapter.setVenuesData(venuesEntities);
            dataLoadedFlag = true;
        }
    }

    @Override
    public void showProgressBar(String description) {
        mProgressBarContainer.setVisibility(View.VISIBLE);
        if (description.length() > 0){
            mProgressBarDescription.setText(description);
        }else{
            mProgressBarDescription.setText("");
        }
    }

    @Override
    public void hideProgressBar() {
        mProgressBarContainer.setVisibility(View.GONE);
    }

    private void myLocationListener() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(command -> {
            LatLng latLng = new LatLng(command.getLatitude(), command.getLongitude());
            if (!dataLoadedFlag){
                mVenuesListPresenter.loadDataByLocation(latLng);
            }else{
                mVenuesListPresenter.refreshData(latLng);
            }
        });
    }

    @Override
    public void showToast(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }
}
