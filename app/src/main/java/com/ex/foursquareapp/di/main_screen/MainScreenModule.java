package com.ex.foursquareapp.di.main_screen;

import com.ex.foursquareapp.ui.main_screen.presenter.IMainScreenPresenter;
import com.ex.foursquareapp.ui.main_screen.presenter.MainScreenPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

@Module
public class MainScreenModule {

    @Provides
    IMainScreenPresenter provideMainScreenPresenter(){
        return new MainScreenPresenter();
    }

}
