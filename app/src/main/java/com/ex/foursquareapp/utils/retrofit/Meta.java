package com.ex.foursquareapp.utils.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public final class Meta {

    @SerializedName("code")
    private int code;
    @SerializedName("requestId")
    private String requestId;

}
