package com.ex.foursquareapp.di.venues_list;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)

public @interface VenuesListScope {
}
