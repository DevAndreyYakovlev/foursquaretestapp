package com.ex.foursquareapp.ui.venues.venues_list.persenter;

import com.ex.foursquareapp.business.venues_list.IVenuesListInteractor;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.ui.venues.venues_list.view.IVenuesListView;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesListPresenter implements IVenuesListPresenter {
    private final IVenuesListInteractor mVenuesListInteractor;
    private IVenuesListView mVenuesListView;
    public VenuesListPresenter(IVenuesListInteractor interactor) {
        mVenuesListInteractor = interactor;
    }

    @Override
    public void bindView(IVenuesListView venuesListFragment) {
        mVenuesListView = venuesListFragment;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void loadDataByLocation(LatLng latLng) {
        mVenuesListView.showProgressBar("Мы Вас нашли, осталось заргузить данные. \nПожалуйста, подождите!");
        mVenuesListInteractor.loadDataByLocation(latLng)
            .subscribe(venuesEntities -> {
                mVenuesListView.showVenues(venuesEntities);
                mVenuesListView.hideProgressBar();
            }, throwable -> {
                mVenuesListView.showToast("Ooops, Что-то пошло не так!");
            });
    }

    @Override
    public boolean venuesDataIsEmpty() {
        return mVenuesListInteractor.venuesDataIsEmpty();
    }

    @Override
    public void loadData() {
        mVenuesListInteractor.loadData()
            .subscribe(venuesEntities -> {
                mVenuesListView.showVenues(venuesEntities);
            });
    }

    @Override
    public void removeVenue(VenuesEntity venuesEntity) {
        mVenuesListInteractor.removeVenue(venuesEntity);
    }

    @Override
    public void refreshData(LatLng latLng) {
        mVenuesListInteractor.loadDataByLocation(latLng)
            .subscribe(venuesEntities -> {
                mVenuesListView.showVenues(venuesEntities);
                mVenuesListView.showToast("Данные обновились!");
            }, throwable -> {
                mVenuesListView.showToast("Ooops, Что-то пошло не так!");
            });
    }
}
