package com.ex.foursquareapp.ui.venues.venues_list.persenter;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.ui.venues.venues_list.view.IVenuesListView;
import com.ex.foursquareapp.ui.venues.venues_list.view.VenuesListFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface IVenuesListPresenter {

    void bindView(IVenuesListView venuesListFragment);
    void unbindView();

    void loadDataByLocation(LatLng latLng);

    boolean venuesDataIsEmpty();

    void loadData();

    void removeVenue(VenuesEntity venuesEntity);

    void refreshData(LatLng latLng);
}
