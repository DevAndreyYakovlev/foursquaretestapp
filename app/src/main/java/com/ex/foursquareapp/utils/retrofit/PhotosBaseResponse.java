package com.ex.foursquareapp.utils.retrofit;

import com.ex.foursquareapp.data.entity.venues_image.Photos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class PhotosBaseResponse<T> {

    @SerializedName("photos")
    private T photos;

    public T getPhotos() {
        return photos;
    }

    public void setPhotos(T photos) {
        this.photos = photos;
    }
}
