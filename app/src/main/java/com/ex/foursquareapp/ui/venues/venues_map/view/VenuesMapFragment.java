package com.ex.foursquareapp.ui.venues.venues_map.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ex.foursquareapp.FoursquareApp;
import com.ex.foursquareapp.R;
import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.di.venues_map.VenuesMapModule;
import com.ex.foursquareapp.ui.venues.venues_map.presenter.IVenuesMapPresenter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenuesMapFragment extends Fragment implements IVenuesMapView, OnMapReadyCallback {

    private View mRootView;
    private GoogleMap mGoogleMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    @BindView(R.id.mapView) MapView mMapView;
    @Inject IVenuesMapPresenter mVenuesMapPresenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FoursquareApp.get(getContext()).applicationComponent().plus(new VenuesMapModule()).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.venues_map_fmt, container, false);
        mVenuesMapPresenter.bindView(this);
        ButterKnife.bind(this, mRootView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);
        return mRootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);
        myLocationListener(googleMap);
        mVenuesMapPresenter.getVenuesData();
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void myLocationListener(GoogleMap googleMap) {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(command -> {
            LatLng latLng = new LatLng(command.getLatitude(), command.getLongitude());
            CameraPosition cameraPos = CameraPosition
                    .builder()
                    .target(latLng)
                    .zoom(14.0f)
                    .build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPos));
        });

    }

    @Override
    public void showPlaceMarks(List<VenuesEntity> venuesEntities) {
        for (VenuesEntity venuesEntity : venuesEntities) {
            if  (venuesEntity.getLocation().getLat() > 0){
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions
                        .position(new LatLng(venuesEntity.getLocation().getLat(), venuesEntity.getLocation().getLng()))
                        .title(venuesEntity.getName());
                mGoogleMap.addMarker(markerOptions);
            }

        }
    }
}
