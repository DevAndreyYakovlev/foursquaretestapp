package com.ex.foursquareapp.ui.venues.venue_window.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ex.foursquareapp.R;
import com.ex.foursquareapp.utils.zoomer.ImageViewTouch;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Yakovlev Andrey. 8/21/2017
 */

public class VenuePhotoZoomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.venue_photo_activity);

        String url = (String) getIntent().getExtras().get("url");



        Picasso.with(this)
                .load(url)
                .into(target);


    }

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            ImageViewTouch imageViewTouch = (ImageViewTouch) findViewById(R.id.imgViewTouch);
            imageViewTouch.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };


}
