package com.ex.foursquareapp.utils.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class BaseResponse<T> {

    @SerializedName("meta")
    private Meta meta;

    @SerializedName("response")
    private T response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
