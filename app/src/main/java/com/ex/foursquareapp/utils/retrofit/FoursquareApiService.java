package com.ex.foursquareapp.utils.retrofit;

import com.ex.foursquareapp.data.entity.venues_image.Photos;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface FoursquareApiService {


    String authToken = "BXEOHJPUQJNOJYNVA4XAZNMVY25ROA3D0GCIJ2IASNVRTHFR";

    @GET("venues/search")
    Observable<BaseResponse<VenuesResponse>> getVenuesList(@Query("ll") String latLng, @Query("oauth_token") String authToken, @Query("v") String date, @Query("locale") String locale);

    @GET("venues/search")
    Observable<BaseResponse<VenuesResponse>> getVenuesList(@QueryMap Map<String, String> json);

    @GET("{id}/photos")
    Observable<BaseResponse<PhotosBaseResponse<Photos>>> getImagesByVenuesId(@Path("id") String id, @QueryMap Map<String, String> json);
}
