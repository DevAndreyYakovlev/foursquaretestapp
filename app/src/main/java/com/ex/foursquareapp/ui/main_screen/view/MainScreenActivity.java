package com.ex.foursquareapp.ui.main_screen.view;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.ex.foursquareapp.FoursquareApp;
import com.ex.foursquareapp.R;
import com.ex.foursquareapp.di.main_screen.MainScreenModule;
import com.ex.foursquareapp.di.venues_list.VenuesListModule;
import com.ex.foursquareapp.ui.main_screen.presenter.IMainScreenPresenter;
import com.ex.foursquareapp.ui.venues.venues_list.view.VenuesListFragment;
import com.ex.foursquareapp.ui.venues.venues_map.view.VenuesMapFragment;
import com.ex.foursquareapp.utils.BaseActivity;
import com.ex.foursquareapp.utils.PermissionRequest;
import com.ex.foursquareapp.utils.RxPermissionListener;
import com.tbruyelle.rxpermissions2.RxPermissions;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class MainScreenActivity extends AppCompatActivity implements IMainScreenView {

    @Inject
    IMainScreenPresenter mMainScreenPresenter;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    private VenuesListFragment mVenuesListFragment;
    private VenuesMapFragment mVenuesMapFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen_activity);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        }

        FoursquareApp.get(getApplicationContext()).applicationComponent().plus(new MainScreenModule()).inject(this);
        mMainScreenPresenter.bindView(this);
        mVenuesListFragment = new VenuesListFragment();
        mVenuesMapFragment = new VenuesMapFragment();
        mFragmentManager = getSupportFragmentManager();
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mMainScreenPresenter.onTabSelected(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mFragmentManager.beginTransaction()
                .add(R.id.fmt_container, mVenuesListFragment, "venuesListFmt")
                .commit();
        checkPermissions();
    }

    private void checkPermissions() {
        new RxPermissions(this)
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if (granted){
                        RxPermissionListener.replaySubject.onNext(true);
                        System.out.println();
                    }else{
                        RxPermissionListener.replaySubject.onNext(false);
                        System.out.println();
                    }
                });
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void showSnackBar(String s) {

    }

    @Override
    public void showListScreen() {
        if (mFragmentManager.findFragmentByTag("venuesListFmt") != null){
            return;
        }
        mFragmentManager.beginTransaction()
                .replace(R.id.fmt_container, mVenuesListFragment, "venuesListFmt")
                .commit();
    }


    @Override
    public void showMapScreen() {
        if (mFragmentManager.findFragmentByTag("venuesMapFmt") != null){
            return;
        }
        mFragmentManager.beginTransaction()
                .replace(R.id.fmt_container, mVenuesMapFragment, "venuesMapFmt")
                .commit();
    }
}
