package com.ex.foursquareapp.di.venue_window;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.ui.venues.venue_window.presenter.IVenueWindowPresenter;
import com.ex.foursquareapp.ui.venues.venue_window.presenter.VenueWindowPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */
@Module
public class VenueWindowModule {

    @Provides
    @VenueWindowScope
    IVenueWindowPresenter provideVenueWindowPresenter(IVenuesInteractor venuesInteractor){
        return new VenueWindowPresenter(venuesInteractor);
    }

}
