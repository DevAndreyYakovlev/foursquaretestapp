package com.ex.foursquareapp.ui.venues.venues_map.view;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;

import java.util.List;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public interface IVenuesMapView {
    void showPlaceMarks(List<VenuesEntity> venuesEntities);
}
