package com.ex.foursquareapp.di.venues_list;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.business.venues.VenuesInteractor;
import com.ex.foursquareapp.business.venues_list.IVenuesListInteractor;
import com.ex.foursquareapp.business.venues_list.VenuesListInteractor;
import com.ex.foursquareapp.ui.venues.venues_list.persenter.IVenuesListPresenter;
import com.ex.foursquareapp.ui.venues.venues_list.persenter.VenuesListPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Module
public class VenuesListModule {

    @Provides
    @VenuesListScope
    IVenuesListPresenter provideVenuesListPresenter(IVenuesListInteractor venuesListInteractor){
        return new VenuesListPresenter(venuesListInteractor);
    }

    @Provides
    @VenuesListScope
    IVenuesListInteractor provideVenuesInteractor(IVenuesInteractor venuesInteractor){
        return new VenuesListInteractor(venuesInteractor);
    }

}
