package com.ex.foursquareapp.utils.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class FoursquareImageFactory {

    private static final int TIMEOUT = 45;
    private static final Gson gson = new GsonBuilder().create();


    static Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/venues/")
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static FoursquareApiService getImages(){
        return getRetrofit().create(FoursquareApiService.class);
    }

    private static OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT,TimeUnit.SECONDS)
                .readTimeout(TIMEOUT,TimeUnit.SECONDS)
                .build();
    }

}
