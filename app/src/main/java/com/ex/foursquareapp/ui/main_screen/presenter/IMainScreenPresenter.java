package com.ex.foursquareapp.ui.main_screen.presenter;

import android.support.design.widget.TabLayout;

import com.ex.foursquareapp.ui.main_screen.view.IMainScreenView;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface IMainScreenPresenter {

    void bindView(IMainScreenView mainScreenView);
    void unbindView();
    void onTabSelected(TabLayout.Tab tab);

}
