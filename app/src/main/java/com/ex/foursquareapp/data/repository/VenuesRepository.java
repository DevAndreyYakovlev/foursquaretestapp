package com.ex.foursquareapp.data.repository;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.ex.foursquareapp.utils.RealmProvider;
import com.ex.foursquareapp.utils.retrofit.BaseResponse;
import com.ex.foursquareapp.utils.retrofit.FoursquareApiService;
import com.ex.foursquareapp.utils.retrofit.FoursquareImageFactory;
import com.ex.foursquareapp.utils.retrofit.VenuesResponse;
import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;


/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class VenuesRepository implements IVenuesRepository {
    private final FoursquareApiService mApiService;
    private final RealmProvider mRealmProvider;

    public VenuesRepository(FoursquareApiService apiService, RealmProvider realmProvider) {
        mApiService = apiService;
        mRealmProvider = realmProvider;
    }

    @Override
    public Observable<List<VenuesEntity>> loadDataByLocation(LatLng latLng) {

        Map<String, String> json = new HashMap<>();
        String ll = latLng.latitude + "," + latLng.longitude;
//        ll = "55.824726,49.135619";
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date today = Calendar.getInstance().getTime();
        String df = dateFormat.format(today);
        json.put("ll", ll);
        json.put("oauth_token", FoursquareApiService.authToken);
        json.put("limit", "10");
        json.put("v", df);
        json.put("locale", "ru");

        return mApiService.getVenuesList(json)
            .map(BaseResponse::getResponse)
            .map(VenuesResponse::getVenuesEntities)
            .flatMap(Observable::fromIterable)
            .flatMap(venuesEntity -> {
                json.clear();
                json. put("oauth_token", FoursquareApiService.authToken);
                json.put("v", df);
                return FoursquareImageFactory.getImages().getImagesByVenuesId(venuesEntity.getId(), json);
            }, (venuesEntity, venuesImageResponseBaseResponse) -> {
                venuesEntity.setVenuesImage(venuesImageResponseBaseResponse.getResponse().getPhotos().getVenuesImages());
                this.saveDataToRealm(venuesEntity);
                return venuesEntity;
            })
            .toList()
            .toObservable()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public boolean venuesDataIsEmpty() {
        return mRealmProvider.provideRealm().where(VenuesEntity.class).count() == 0;
    }

    @Override
    public Observable<List<VenuesEntity>> loadData() {
        return Observable.just(mRealmProvider.provideRealm().where(VenuesEntity.class).findAll());
    }

    @Override
    public Observable<VenuesEntity> loadVenueById(String id) {
        Realm realm = mRealmProvider.provideRealm();
        return Observable.just(realm.copyFromRealm(realm.where(VenuesEntity.class).equalTo("id", id).findFirst()));
    }

    @Override
    public void removeVenue(VenuesEntity venuesEntity) {
        Realm realm = mRealmProvider.provideRealm();
        realm.beginTransaction();
        realm.where(VenuesEntity.class).equalTo("id", venuesEntity.getId()).findFirst().deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    private void saveDataListToRealm(List<VenuesEntity> venuesEntities){
        Realm realm = mRealmProvider.provideRealm();
        realm.beginTransaction();
        realm.insertOrUpdate(venuesEntities);
        realm.commitTransaction();
        realm.close();
    }

    private void saveDataToRealm(VenuesEntity venuesEntity){
        Realm realm = mRealmProvider.provideRealm();
        realm.beginTransaction();
        realm.insertOrUpdate(venuesEntity);
        realm.commitTransaction();
        realm.close();
    }

}
