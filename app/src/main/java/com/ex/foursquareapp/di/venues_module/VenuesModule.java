package com.ex.foursquareapp.di.venues_module;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.business.venues.VenuesInteractor;
import com.ex.foursquareapp.data.repository.IVenuesRepository;
import com.ex.foursquareapp.data.repository.VenuesRepository;
import com.ex.foursquareapp.utils.RealmProvider;
import com.ex.foursquareapp.utils.retrofit.FoursquareApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */
@Module
public class VenuesModule {

    @Provides
    @Singleton
    IVenuesInteractor provideVenuesInteractor(IVenuesRepository venuesRepository){
        return new VenuesInteractor(venuesRepository);
    }

    @Provides
    @Singleton
    IVenuesRepository provideVenuesRepository(FoursquareApiService apiService, RealmProvider realmProvider){
        return new VenuesRepository(apiService, realmProvider);
    }

}
