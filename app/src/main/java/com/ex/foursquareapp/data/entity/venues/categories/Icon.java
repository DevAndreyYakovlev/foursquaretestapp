package com.ex.foursquareapp.data.entity.venues.categories;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class Icon extends RealmObject {

    @SerializedName("prefix")
    private String prefix;
    @SerializedName("suffix")
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
