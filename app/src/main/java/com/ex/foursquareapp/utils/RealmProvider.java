package com.ex.foursquareapp.utils;

import io.realm.Realm;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public class RealmProvider {

    public RealmProvider() {
    }

    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

}
