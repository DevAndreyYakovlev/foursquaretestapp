package com.ex.foursquareapp.ui.venues.venue_window.presenter;

import com.ex.foursquareapp.business.venues.IVenuesInteractor;
import com.ex.foursquareapp.ui.venues.venue_window.view.IVenueWindowView;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenueWindowPresenter implements IVenueWindowPresenter {


    private final IVenuesInteractor mVenuesInteractor;
    private IVenueWindowView mVenueWindowView;


    public VenueWindowPresenter(IVenuesInteractor venuesInteractor) {
        mVenuesInteractor = venuesInteractor;
    }

    @Override
    public void bindView(IVenueWindowView venueWindowView) {
        mVenueWindowView = venueWindowView;
    }

    @Override
    public void unbindView() {

    }

    @Override
    public void getVenueDataById(String id) {
        mVenuesInteractor.loadVenueById(id)
            .subscribe(venuesEntity -> {
                mVenueWindowView.showVenueData(venuesEntity);
            }, throwable -> {
                System.out.println();
            });
    }
}
