package com.ex.foursquareapp.utils.retrofit;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Yakovlev Andrey. 8/20/2017
 */

public class VenuesResponse {

    @SerializedName("venues") ArrayList<VenuesEntity> venuesEntities;

    public ArrayList<VenuesEntity> getVenuesEntities() {
        return venuesEntities;
    }

    public void setVenuesEntities(ArrayList<VenuesEntity> venuesEntities) {
        this.venuesEntities = venuesEntities;
    }
}
