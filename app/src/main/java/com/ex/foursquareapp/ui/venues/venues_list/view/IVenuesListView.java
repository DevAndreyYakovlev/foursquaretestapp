package com.ex.foursquareapp.ui.venues.venues_list.view;

import com.ex.foursquareapp.data.entity.venues.VenuesEntity;

import java.util.List;

/**
 * Created by Yakovlev Andrey. 8/19/2017
 */

public interface IVenuesListView {
    void showVenues(List<VenuesEntity> venuesEntities);

    void showProgressBar(String description);
    void hideProgressBar();

    void showToast(String s);
}
